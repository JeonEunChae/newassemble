﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.UI;

public class AssembleButton : MonoBehaviour
{
    public static int currentAssembleNum = 0;
    public static int productNum;
    [SerializeField]
    GameObject itemPanel;
    [SerializeField]
    GameObject descriptionPanel;
    [SerializeField]
    GameObject[] product;

    AssembleItem[] items;
    static bool isPanelLoading;
    public static bool isAssembling;
    static bool isAssembleMode = true;
    

    void Start()
    {
        currentAssembleNum = 0;
        GameObject productSet = Instantiate(product[GameMaster.Instance.productNum]);
        items = productSet.GetComponentsInChildren<AssembleItem>();
        items[currentAssembleNum].ReadyAssemble();
    }
    public void OnClickAssemble(int buttonNum)
    {
        if (currentAssembleNum == buttonNum && !isPanelLoading)
        {
            StartCoroutine(ClickAssemble());
        }
    }
    IEnumerator ClickAssemble()
    {
        isPanelLoading = !isPanelLoading;
        items[currentAssembleNum].Assemble();
        iTween.MoveBy(itemPanel, iTween.Hash("y", -Screen.height * 0.4f, "easeType", iTween.EaseType.easeInOutQuad, "time", .5f));
        iTween.MoveBy(descriptionPanel, iTween.Hash("y", -Screen.height, "easeType", iTween.EaseType.easeInOutQuad, "time", .5f));
        yield return new WaitForSeconds(.5f);
        currentAssembleNum++;
        isAssembleMode = true;
        isPanelLoading = !isPanelLoading;

    }

    public void OnClickNext()
    {
        if (!isPanelLoading && currentAssembleNum < items.Length)
        {
            StartCoroutine(ClickNext());
        }
        else if(currentAssembleNum == items.Length)
        {
            SceneMaster.Instance.SceneLoad("BlockCoding");
        }
    }
    IEnumerator ClickNext()
    {
        isPanelLoading = !isPanelLoading;
        items[currentAssembleNum].ReadyAssemble();
        iTween.MoveBy(itemPanel, iTween.Hash("y", Screen.height * 0.4f, "easeType", iTween.EaseType.easeInOutQuad, "time", .5f));
        iTween.MoveBy(descriptionPanel, iTween.Hash("y", Screen.height, "easeType", iTween.EaseType.easeInOutQuad, "time", .5f));
        yield return new WaitForSeconds(.5f);
        isAssembleMode = false;
        isPanelLoading = !isPanelLoading;
    }

    public void OnClickUndo()
    {
        if (currentAssembleNum > 0 && !isAssembling)
        {
            currentAssembleNum--;
            items[currentAssembleNum].HideItem();
            if(isAssembleMode) StartCoroutine(ClickNext());
            else items[currentAssembleNum].ReadyAssemble();
            items[currentAssembleNum + 1].HideItem();
        }
        
    }
    public void OnClickHome()
    {
        SceneMaster.Instance.SceneLoad("Loby");
    }
}
