﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssembleItem : MonoBehaviour
{
    //public int assembleNum;
    Vector3 originPos;
    Renderer rend;
    Color hideColor;
    Color readyColor;
    Color originColor;

    [SerializeField]
    GameObject[] lines;
    [SerializeField]
    GameObject electricity;
    Color[] lineOriginColor;
    Vector3[] lineOriginPos;


    void Awake()
    {
        originPos = transform.position;
        rend = GetComponent<Renderer>();
        hideColor = new Color(0f, 0f, 0f, 0f);
        readyColor = new Color(1f, 1f, 1f, 0.5f);
        originColor = rend.material.color;

        lineOriginColor = new Color[lines.Length];
        lineOriginPos = new Vector3[lines.Length];
        for (int i = 0; i < lines.Length; i++)
        {
            lineOriginColor[i] = lines[i].GetComponent<Renderer>().material.color;
            lineOriginPos[i] = lines[i].transform.position;
        }

        HideItem();

    }

    public void ReadyAssemble()
    {
        rend.material.color = readyColor;
    }

    public void HideItem()
    {
        rend.material.color = hideColor;
        for (int i = 0; i < lines.Length; i++)
        {
            lines[i].GetComponent<Renderer>().material.color = hideColor;
            lines[i].transform.position = lineOriginPos[i];
        }
        if (electricity != null)
        {
            electricity.SetActive(false);
        }
    }

    public void Assemble()
    {
        StartCoroutine(AssembleCoroutine());
    }

    public void ParticleOneShot(Vector3 pos)
    {
        AssembleParticle.Instance.particle.transform.position = pos;
        AssembleParticle.Instance.OneShot();
    }

    IEnumerator AssembleCoroutine()
    {
        AssembleButton.isAssembling = true;
        rend.material.color = originColor;
        transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f));
        iTween.MoveTo(gameObject, iTween.Hash("position", originPos, "easeType", iTween.EaseType.easeInOutQuad, "time", .5f));
        yield return new WaitForSeconds(.5f);
        ParticleOneShot(originPos);

        StartCoroutine(Lining());
        yield return new WaitForSeconds(.6f);
        StartCoroutine(Operation());
        yield return new WaitForSeconds(1.6f);
        AssembleButton.isAssembling = false;
    }

    IEnumerator Lining()
    {
        for (int i = 0; i < 50; i++)
        {
            for (int j = 0; j < lines.Length; j++)
            {
                lines[j].GetComponent<Renderer>().material.color += lineOriginColor[j] * 0.02f;//new Color( lineOriginColor[i].r,lineOriginColor[i].g,lineOriginColor[i].b,lineOriginColor[i].a) * 0.02f ;
            }
            yield return null;
        }
        for (int i = 0; i < lines.Length; i++)
        {
            iTween.MoveBy(lines[i], iTween.Hash("z", 1f, "easeType", iTween.EaseType.easeInOutQuad, "time", .5f));
            //yield return new WaitForSeconds(.5f);
        }
    }

    IEnumerator Operation()
    {
        if (electricity != null)
        {
            electricity.SetActive(true);
            yield return new WaitForSeconds(1.6f);
            gameObject.GetComponent<Renderer>().material.color = new Color(0f, 1f, 0f, 1f);
        }
    }
}

