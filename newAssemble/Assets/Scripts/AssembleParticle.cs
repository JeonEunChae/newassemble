﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssembleParticle : MonoBehaviour
{

	public static AssembleParticle Instance;
    public ParticleSystem particle;
    
	void Awake()
	{
		if(Instance == null) Instance = this;
		particle = GetComponent<ParticleSystem>();
	}
	public void OneShot()
	{
//		Debug.Log("oneshot");
		particle.Emit(30);
	}
	
}
