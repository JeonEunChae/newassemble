﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Block : MonoBehaviour
{


    public int currentNum = 0;
    public enum BlockType
    {
        Function,
        Basic
    }
    public BlockType type;
    Image image;
    void Awake()
    {
        image = GetComponent<Image>();
    }

    public void OnClickBlock()
    {
        currentNum++;
        switch (type)
        {
            case BlockType.Function:
                if (currentNum == BlockCodingButton.Instance.funcMax) currentNum = 0;
                image.sprite = BlockCodingButton.Instance.funcSprites[currentNum];
                break;

            case BlockType.Basic:
                if (currentNum == BlockCodingButton.Instance.basicMax) currentNum = 0;
                image.sprite = BlockCodingButton.Instance.basicSprites[currentNum];
                break;

            default:
                break;
        }
    }
}
