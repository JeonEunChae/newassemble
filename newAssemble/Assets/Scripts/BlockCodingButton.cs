﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockCodingButton : MonoBehaviour {

	
	public int funcMax = 4;	
	public int basicMax = 2;	
	public Sprite[] funcSprites;
	public Sprite[] basicSprites;

	public static BlockCodingButton Instance;
	void Awake()
	{
		if(Instance == null) Instance = this;
		if(basicSprites.Length != basicMax || funcSprites.Length != funcMax) Debug.LogWarning("Number of sprites is not match");
	}

	public void OnClickFinish()
	{
		GameMaster.Instance.CompleteProduct();
		SceneMaster.Instance.SceneLoad("Loby");
	}

	public void OnClickHome()
	{
		SceneMaster.Instance.SceneLoad("Loby");
	}

	public void OnClickBlock()
	{
		
	}
}
