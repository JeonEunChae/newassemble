﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameMaster : MonoBehaviour {

	public int productNum = 0;
	public bool[] completedProduct;
	public static GameMaster Instance;
	void Awake()
	{
		if(Instance == null) Instance = this;
		else Destroy(gameObject);
		DontDestroyOnLoad(gameObject);

		completedProduct = new bool[7];
	}

	public void CompleteProduct()
	{
		completedProduct[productNum] = true;
	}


}
