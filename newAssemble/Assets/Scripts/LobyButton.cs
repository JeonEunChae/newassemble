﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobyButton : MonoBehaviour
{

    [SerializeField]
    LobyItem[] items;
    [SerializeField]
    GameObject itemPanel;
    [SerializeField]
    GameObject descriptionPanel;
	[SerializeField]
	GameObject completeParticle;

    bool isPanelLoading;
    int currentNum = 0;

	Color incompletedColor;
	Color completedColor;
	void Start()
	{
		if(GameMaster.Instance.completedProduct[0]) completeParticle.GetComponent<ParticleSystem>().Play();
		else completeParticle.GetComponent<ParticleSystem>().Stop();
		incompletedColor = new Color(0.5f, 0.5f, 1f, 0.5f);
		completedColor = new Color(1f, 1f, 1f, 1f);

		for(int i = 0; i< items.Length; i ++)
		{
			if(GameMaster.Instance.completedProduct[i])
			{
				items[i].gameObject.GetComponent<Renderer>().material.color = completedColor;
				//Instantiate(completeParticle, items[i].transform, true);
			}
			else{
				items[i].gameObject.GetComponent<Renderer>().material.color = incompletedColor;
			}
		}
	}
    public void OnClickItem(int num)
    {
        if (!isPanelLoading && num != currentNum)
        {
            StartCoroutine(ClickItem(num));
        }
    }

    IEnumerator ClickItem(int num)
    {
        isPanelLoading = !isPanelLoading;
		GameMaster.Instance.productNum = num;
		
        //iTween.MoveBy(itemPanel, iTween.Hash("x", -Screen.height * 0.4f, "easeType", iTween.EaseType.easeInOutQuad, "time", .5f));
        descriptionPanel.transform.Translate(Screen.height * 0.7f, 0f, 0f);
        iTween.MoveBy(descriptionPanel, iTween.Hash("x", -Screen.height * 0.7f, "easeType", iTween.EaseType.easeInOutQuad, "time", .5f));
        

        iTween.MoveTo(items[num].gameObject, iTween.Hash("y", 0f, "easeType", iTween.EaseType.easeInOutQuad, "time", .5f));
        iTween.MoveTo(items[currentNum].gameObject, iTween.Hash("y", -20f, "easeType", iTween.EaseType.easeInOutQuad, "time", .5f));
        yield return new WaitForSeconds(0.6f);
		items[currentNum].transform.Translate(0f,40f,0f, Space.World);

		if(GameMaster.Instance.completedProduct[num]) completeParticle.GetComponent<ParticleSystem>().Play();
		else completeParticle.GetComponent<ParticleSystem>().Stop();
        currentNum = num;
		isPanelLoading = !isPanelLoading;
    }

    public void OnClickCancel()
    {
        if (!isPanelLoading)
        {
            StartCoroutine(ClickCancel());
        }
    }

    IEnumerator ClickCancel()
    {
        isPanelLoading = !isPanelLoading;
        //iTween.MoveBy(itemPanel, iTween.Hash("x", Screen.height * 0.4f, "easeType", iTween.EaseType.easeInOutQuad, "time", .5f));
        descriptionPanel.transform.Translate(-Screen.height * 0.7f, 0f, 0f);
        iTween.MoveBy(descriptionPanel, iTween.Hash("x", Screen.height * 0.7f, "easeType", iTween.EaseType.easeInOutQuad, "time", .5f));
        yield return new WaitForSeconds(.5f);
        isPanelLoading = !isPanelLoading;
    }

    public void OnClickGo()
    {
        SceneMaster.Instance.SceneLoad("Assemble");
    }



}
