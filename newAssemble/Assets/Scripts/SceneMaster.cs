﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneMaster : MonoBehaviour {

	public static SceneMaster Instance;
	void Awake()
	{
		if(Instance == null) Instance = this;
		else Destroy(gameObject);
		DontDestroyOnLoad(gameObject);
	}

	public void SceneLoad(string name)
	{
		SceneManager.LoadScene(name, LoadSceneMode.Single);
	}

}
